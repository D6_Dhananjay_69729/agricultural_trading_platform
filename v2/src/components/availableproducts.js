import axios from 'axios';
import React, { Component, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
function AvailableProducts() {

const [plist,setPlist]=useState([])
var [searchText,setsearchText] = useState("");
const navigate = useNavigate()
useEffect(()=>{
    getProducts()
},[])
var  Search=(args)=>{
    setsearchText(args.target.value);
}

const getProducts=()=>{
    const id=4;
    axios.get("http://localhost:8080/buyer/availableproduct")
    .then(response=>{
        const result =response.data
        
        if (result['status'] === 'error') {
            toast.error(result['error'])
          } else {
            console.log(result)
            setPlist(result)
          }
       
    }).catch(err=>{console.log(err)})
}

const deleteProduct=(id)=>{
        
    axios.delete("http://localhost:8080/buyer/afterorderexecution/"+id)
    .then((response)=>{
        const result =response.data         
        if(result['status'] === 'error'){
            toast.error("unable to delete")
           
        }else{
            
            toast.success("Order Confirmed")
            getProducts()
        }
    }).catch(err=>{console.log(err)})
}

    return (  
        <>
         <div>
            <div>
                <h3>Search For AvailableProducts</h3>
                <div>
                    <input type='text' value={searchText} placeholder='Search Product Name' onChange={Search}/>
                </div>
            </div>
            <table > 
                <thead>
                    <tr> 
                        <td>Product Name</td>
                        <td>Price</td>
                        <td>Quantity</td>
                        <td>Expected Date</td>
        
                    </tr>
                </thead>
                <tbody>
                    {
                        plist.map((p)=>{
                            if(p.productName.toLowerCase().includes
                            (searchText.toLowerCase()))
                            return(
                                <tr key={p.id}>
                                <td>{p.productName}</td>
                                <td>{p.price}</td>
                                <td>{p.quantity}</td>
                                <td>{p.expectedDate}</td>
                                <td><button className='btn btn-primary' onClick={()=>deleteProduct(p.id)} >Buy</button></td>
                 
                            </tr>
                            )
                        }
                        )
                    }
                </tbody>

            </table>

        </div>
        </>
    );
}

export default AvailableProducts;