import axios from "axios"
import { Button } from "bootstrap";
import { Component, useState } from "react"
import { useNavigate } from "react-router-dom";


function AddProduct(){

    const[ProductName,setProductName]=useState('')
    const[quantity,setQuantity]=useState('')
    const[price,setPrice]=useState('')
    const[expectedDate,setExpectedDate]=useState('')
    const buyOSell="sell";

    const product={
        ProductName,
        quantity,
        price,
        expectedDate,
        buyOSell
    }
    const navigate = useNavigate();

    var Add_Product=()=>{
        axios.post("http://localhost:8080/seller/addproduct/4",product)
        .then(result=>{
            navigate('/product')
        }).catch(err=>{console.log(err)})
    }
    

    return(
        <div>
            <table>
                <tr>
                    <td>Product</td>
                    <td><input type="text" name="ProductName" 
                    onChange={(e)=>{setProductName(e.target.value)}}/></td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td><input type="number" name="price" value={product.price}
                    onChange={(e)=>{setPrice(e.target.value)}}/></td>
                </tr>
                <tr>
                    <td>Quantity</td>
                    <td><input type="number" name="quantity"
                    onChange={(e)=>{setQuantity(e.target.value)}}/></td>
                </tr>
                <tr>
                    <td>Expected date</td>
                    <td><input type="date" name="expecteddate"
                    onChange={(e)=>{setExpectedDate(e.target.value)}}/></td>
                </tr>
                <tr>
                    <td colSpan={2}>
                        <button className='btn btn-success' onChange={Add_Product}>Add Product</button>
                    </td>
                </tr>
            </table>
        </div>
    );
}

export default AddProduct;