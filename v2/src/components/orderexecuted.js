import axios from 'axios';
import React, { Component, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

function OrderExecuted() {
    const [olist,setOlist]=useState([])
    useEffect(()=>{
        getMyOrders()
    },[])

    const navigate=useNavigate();

    const getMyOrders=()=>{
        const id=6;
        axios.get("http://localhost:8080/buyer/getmyorders/"+id)
        .then(response=>{
            const result =response.data
            if(result['status'] === 'error'){
                toast.error(result['error'])
            }
            else{
                console.log(result)
                setOlist(result)
            }
        }).catch(err=>{console.log(err)})
    }

     
  const getTrn = (id) => {
    
    navigate('/transactions', { state: { orderId: id } })
  }

    return ( 
        <>
         <div>
            <table > 
                <thead>
                    <tr> 
                        <td>Product Traded </td>
                        <td>Quantity</td>
                        <td>Amount</td>
                        <td>Order Date</td>
                        <td>Expected Date</td>
        
                    </tr>
                </thead>
                <tbody>
                    {
                        olist.map((o)=>{
                            return(
                                <tr key={o.id}>
                                <td>{o.productTraded}</td>
                                <td>{o.quantity}</td>
                                <td>{o.amount}</td>
                                <td>{o.orderDate}</td>
                                <td>{o.expectedDate}</td>
                                <td><button className='btn btn-primary' onClick={() => getTrn(o.id)} >Transaction Details</button></td>
                                
                
                            </tr>
                            )
                        }
                        )
                    }
                </tbody>

            </table>

        </div>

        
        </>
     );
}

export default OrderExecuted;