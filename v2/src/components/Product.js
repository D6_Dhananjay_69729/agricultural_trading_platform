import axios from 'axios';
import React, {useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
const Product=()=> {
    const [listings, setListings] = useState([])

    const navigate=useNavigate()
    

    useEffect(()=>{
        getMyProducts()
    },[])
    
    const getMyProducts=()=>{
        const id=4;
        axios.get("http://localhost:8080/buyer/myproduct/"+id)
        .then(response=>{
            const result =response.data
            // setListings(response.data)
            if (result['status'] === 'error') {
                toast.error(result['error'])
              } else {
                console.log(result)
                setListings(result)
              }
           
        }).catch(err=>{console.log(err)})
    }
    
    const gheunja=()=>{
       navigate('/addproduct')
    }
    
    const deleteProduct=(id)=>{
        
        axios.delete("http://localhost:8080/buyer/deleteproduct/"+id)
        .then((response)=>{
            const result =response.data         
            if(result['status'] === 'error'){
                toast.error("unable to delete")
              //getMyProducts()
            }else{
                
                toast.success("product deleted")
                getMyProducts()
            }
        }).catch(err=>{console.log(err)})
    }

    return ( 
        <>
        <div>
            <table > 
                <thead>
                    <tr> 
                        <td>Product Name</td>
                        <td>Price</td>
                        <td>Quantity</td>
                        <td>Expected Date</td>
        
                    </tr>
                </thead>
                <tbody>
                    {
                        listings.map((p)=>{
                            return(
                                <tr key={p.id}>
                                <td>{p.productName}</td>
                                <td>{p.price}</td>
                                <td>{p.quantity}</td>
                                <td>{p.expectedDate}</td>
                                <td><button className='btn btn-primary' onClick={gheunja}>Add</button></td>
                                <td><button className='btn btn-danger' onClick={()=>deleteProduct(p.id) }>Delete</button></td>
                
                            </tr>
                            )
                        }
                        )
                    }
                </tbody>

            </table>

        </div>
        </>
     );
}

export default Product;