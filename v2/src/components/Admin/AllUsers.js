import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';

const AllUsers = () => {

    const [users, setUsers] = useState([]);
    const[role,setRole]=useState('')
    useEffect(() => { getAllUsers() }, []);
    const getAllUsers= () => {
        axios
        .get("http://localhost:8080/admin/getallusers")
        .then((response)=> {
            const result = response.data
            console.log(result)
            if (result['status'] === 'error') {
                toast.error(result['error'])
            }
            else {
                setUsers(result)
                // setUsers(result)
                // console.log(users)
            }
        }).catch(err => { console.log(err) })
        debugger
    }

    const deleteUser = (id) => {
        axios.delete("http://localhost:8080/deleteuser/" + id).then((response) => {
            const result = response.data

            if (result['status'] === 'error') {
                toast.error("Unable to delete user")
            }
            else {
                toast.success("User deleted");
                getAllUsers();
            }
        }).catch(err => { console.log(err) })
    }
const getRole=()=>{
        debugger
        const getrole=document.getElementById("role")
        if(getrole.value==='1'){
            setRole('admin')
        }else if(getrole.value==='2'){
            setRole('seller')
        }else if(getrole==='3'){
            setRole('buyer')
        }
}

    return (
        <div>
            <select name='selectRole' id='role' onChange={getRole}>
                <option value={0}>Select Role</option>
               <option value={1}> Admin</option>
                <option value={2}>Seller</option>
                <option value={3}>Buyer</option>
            </select>
            <table>
                <thead>
                    <tr>
                        <td>User ID</td>
                        <td>Role</td>
                        <td>Title</td>
                        <td>First Name</td>
                        <td>Middle Name</td>
                        <td>Last Name</td>
                        <td>Gender</td>
                        <td>Date of Birth</td>
                        <td>Registration Date</td>
                        <td>Mobile Number</td>
                        <td>E-mail ID</td>
                        <td>ID Type</td>
                        <td>ID Number</td>
                        <td>Address</td>
                        <td>Village</td>
                        <td>District</td>
                        <td>State</td>
                        <td>Pincode</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        users.map((s) => {
                            // if(s.role.toLowerCase.includes(role.toLowerCase)){
                            return (
                                    <tr key={s.id}>
                                    <td>{s.id}</td>
                                    <td>{s.role}</td>
                                    <td>{s.title}</td>
                                    <td>{s.first_name}</td>
                                    <td>{s.middle_name}</td>
                                    <td>{s.last_name}</td>
                                    <td>{s.gender}</td>
                                    <td>{s.dob}</td>
                                    <td>{s.registration_date}</td>
                                    <td>{s.mobile_number}</td>
                                    <td>{s.email}</td>
                                    <td>{s.photo_id_type}</td>
                                    <td>{s.photo_id_number}</td>
                                    <td>{s.address}</td>
                                    <td>{s.village}</td>
                                    <td>{s.district}</td>
                                    <td>{s.state}</td>
                                    <td>{s.pincode}</td>
                                    <td><button className='btn btn-danger' onClick={() => deleteUser(s.id)}>Delete User</button></td>
                                </tr>
                            )
                            // }
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AllUsers
