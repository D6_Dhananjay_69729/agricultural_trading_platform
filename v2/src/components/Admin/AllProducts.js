import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const AllProducts = () => {

    const [Products, setProducts] = useState([]);
    useEffect(() => { getAllProducts() }, []);

    const getAllProducts = () => {
        axios.get("http://localhost:8080/admin/product").then(response => {
            const result = response.data
            if (result['status'] === 'error') {
                toast.error(result['error'])
            }
            else {
                setProducts(result)
            }
        }).catch(err => { console.log(err) })
    }


    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <td>Product ID</td>
                        <td>Product Name</td>
                        <td>Quantity</td>
                        <td>Price</td>
                        <td>Expected Date</td>
                        <td>Buy/Sell</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        Products.map((p) => {
                            return (
                                <tr key={p.productId}>
                                    <td>{p.productId}</td>
                                    <td>{p.product_name}</td>
                                    <td>{p.quantity}</td>
                                    <td>{p.price}</td>
                                    <td>{p.expected_date}</td>
                                    <td>{p.buy_sell}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AllProducts
