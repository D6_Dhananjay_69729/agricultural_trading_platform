import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const AllTransactions = () => {

    const [Transcactions, setTransactions] = useState([]);
    useEffect(() => { getAllTransactions() }, []);

    const getAllTransactions = () => {
        axios.get("http://localhost:8080/admin/getalltransactions").then(respone => {
            const result = respone.data
            if (result['status'] === 'error') {
                toast.error(result['error'])
            }
            else {
                setTransactions(result)
            }
        }).catch(err => { console.log(err) })
    }

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <td>Transaction ID</td>
                        <td>Transaction Number</td>
                        <td>Transaction Amount</td>
                        <td>Transaction Date</td>
                        <td>Order ID</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        Transcactions.map((t) => {
                            return (
                                <tr key={t.transactionId}>
                                    <td>{t.transactionId}</td>
                                    <td>{t.transaction_number}</td>
                                    <td>{t.transaction_amount}</td>
                                    <td>{t.date_of_transaction}</td>
                                    <td>{t.orderId}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AllTransactions


