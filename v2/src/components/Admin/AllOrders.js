import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { toast } from 'react-toastify';

const AllOrders = () => {

    const [Orders, setOrders] = useState([]);
    useEffect(() => { getAllOrders() }, []);

    const getAllOrders = () => {
        axios.get("http://localhost:8080/admin/getallorderexecuted").then(response => {
            const result = response.data
            if (result = ['status'] === 'error') {
                toast.error(result['error'])
            }
            else {
                setOrders(result)
            }
        }).catch(err => { console.log(err) })
    }

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <td>Order ID</td>
                        <td>Order Date</td>
                        <td>Order Amount</td>
                        <td>Product Traded</td>
                        <td>Quantity</td>
                        <td>Expected Date</td>
                        <td>User ID</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        Orders.map((o) => {
                            return (
                                <tr key={o.orderId}>
                                    <td>{o.orderId}</td>
                                    <td>{o.order_date}</td>
                                    <td>{o.amount}</td>
                                    <td>{o.product_traded}</td>
                                    <td>{o.quantity}</td>
                                    <td>{o.expected_date}</td>
                                    <td>{o.userId}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AllOrders
