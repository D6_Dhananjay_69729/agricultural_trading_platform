import axios from "axios";
import { Button } from "bootstrap";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { Container } from "reactstrap";

function ShowMyProducts(){
     const [listings, setListings] = useState([]);
     const navigate = useNavigate();

     useEffect(()=>{
        GetMyProducts()
     },[]);

     const GetMyProducts=()=>{
        const id=5;
        axios.get("http://localhost:8080/seller/myproduct/"+id)
        .then(response=>{
            const result=response.data
            if(result['status']==='error'){
                toast.error(result['error'])
            }
            else{
                console.log(result);
                setListings(result);
            }
        }).catch(err=>{console.log(err)})
     }

     const Add=()=>{
        navigate('/addproduct')
     }

     const DeleteProduct=()=>{
        axios.delete("http://localhost:8080/seller/deleteproduct/"+id)
        .then((response)=>{
            const result =response.data         
            if(result['status'] === 'error'){
                toast.error("unable to delete")
            }
            else{
                toast.success("product deleted")
                getMyProducts()
            }
        }).catch(err=>{console.log(err)})
     }

    return(
        <div>
            <button onClick={Add} className='btn btn-primary'>Add Product</button>
            
            <table>
                <thread>
                    <tr>
                        <td>Product Name</td>
                        <td>Price</td>
                        <td>Quantity</td>
                        <td>Expected Date</td>
                    </tr>
                </thread>

                <tbody>
                    {
                        listings.map=((x)=>{
                            return(
                                <tr key={x.id}>
                                    <td>{x.ProductName}</td>
                                    <td>{x.price}</td>
                                    <td>{x.quantity}</td>
                                    <td>{x.expectedDate}</td>
                                    <td><button className='btn btn-danger' onClick={DeleteProduct} ></button></td>
                                </tr>
                            );
                        }
                        )
                    }
                </tbody>
            </table>
        </div>
        );
}

export default ShowMyProducts;
