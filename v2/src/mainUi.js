import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./footer";
import Header from "./headder";
import Home from "./home";
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Product from "./components/Product";
import AddProduct from "./components/addproduct";
import AvailableProducts from "./components/availableproducts";
import OrderExecuted from "./components/orderexecuted";
import Transaction from "./components/transaction";
import SellerProduct from "./sellerComponent/sellerProduct";
import SellerAddProduct from "./sellerComponent/sellerAddproduct";
import SellerReqProducts from "./sellerComponent/sellerReqproducts";
import SellerTransaction from "./sellerComponent/sellerTransaction";
import SellerOrderExecuted from "./sellerComponent/sellerOrderexecuted";

function MainUi() {
    return (
        <>
        <BrowserRouter>
       
            <Header></Header>
                <Routes>
                <Route path='/'  element={<Home/>}></Route>
                <Route path='/product'  element={<Product/>}></Route>
                <Route path='/addproduct' element={<AddProduct/>}></Route>
                <Route path='/availableproducts' element={<AvailableProducts/>}></Route>
                <Route path='/orders' element={<OrderExecuted/>}></Route>
                <Route path='/transactions' element={<Transaction/>}></Route>
                <Route path='/sellermyproduct' element={<SellerProduct/>}></Route>
                <Route path='/selleraddproduct' element={<SellerAddProduct/>}></Route>
                <Route path='/selleReqproducts' element={<SellerReqProducts/>}></Route>
                <Route path='/sellerorders' element={<SellerOrderExecuted/>}></Route>
                <Route path='/sellertransaction' element={<SellerTransaction/>}></Route>
                </Routes>
            <Footer></Footer>
            </BrowserRouter>
        </> 
     );
}

export default MainUi;