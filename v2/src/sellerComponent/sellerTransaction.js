import axios from 'axios';
import React, { Component, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
function SellerTransaction() {
    
const [tlist,setTlist] = useState([])

const location = useLocation()
const navigate = useNavigate()

useEffect(()=>{
    const { orderId } = location.state
    getMyTransaction(orderId)
},[])

const getMyTransaction=(id)=>{
    // const id=4;
    axios.get("http://localhost:8080/seller/gettransaction/"+id)
    .then(response=>{
        const result =response.data
        if (result['status'] === 'error') {
            toast.error(result['error'])
          } else {
            console.log(result)
            setTlist(result)
          }
    }).catch(err=>{console.log(err)})
}

    return ( <>
     <div>
            <table > 
                <thead>
                    <tr> 
                        <td>Transaction Number</td>
                        <td>Amount</td>                        
                        <td>Transaction Date</td>        
                    </tr>
                </thead>
                <tbody>
                   
                                <tr key={tlist.id}>
                                <td>{tlist.trnNumber}</td>
                                <td>{tlist.amount}</td>
                                <td>{tlist.trnDate}</td>                             
                                </tr>
                            
                        
                        
                    
                </tbody>

            </table>

        </div>
    </> );
}

export default SellerTransaction;