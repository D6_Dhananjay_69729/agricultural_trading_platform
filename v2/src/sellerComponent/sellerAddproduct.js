import axios from 'axios';
import React, {  useState } from 'react';
import { useNavigate } from 'react-router-dom';
function SellerAddProduct() {
    const [productName,setProductName]=useState('')
    const[quantity,setquantity]=useState('')
    const[price,setprice]=useState('')
    const[expectedDate,setexpectedDate]=useState('')
    const buyOSell= "sell"
    
    const product={
        productName,
        quantity,
        price,
        expectedDate,
        buyOSell
    }
   const navigate= useNavigate()

  var sendProduct=()=>{
    const id=6;
    axios
    .post("http://localhost:8080/seller/addproduct/"+id,product)
    .then(result=>{
        navigate('/sellermyproduct')
    }).catch(err=>{console.log(err)})
  } 
    return ( <>
     <div>
                <div>
                <label>
                    Product Name :
                </label>
                <input type="text" name='productName'
                 onChange={(e)=>{setProductName(e.target.value)}}/>
                </div>
                <br/>
                <div>
                <label>
                    Price :
                    <br/>
                </label>
                <input type="number" name='price' value={product.price} 
                onChange={(e)=>{setprice(e.target.value)}}></input>
                </div>
                <br/>
                <div>
                <label>
                    Quantity :
                   
                </label>
                <input type="number" name='quantity'  
                onChange={(e)=>{setquantity(e.target.value)}}></input>
                </div>
                <br/>
                <div>
                <label>
                    Expected Date :
                </label>
                <input type="date" name='expectedDate'  
                onChange={(e)=>{setexpectedDate(e.target.value)}}></input>
                </div><br/>
                <button className='btn btn-success' onClick={sendProduct}>Add Required Product

                </button>
        </div>
    </> );
}

export default SellerAddProduct;