package com.example.demo.service;

import java.util.List;

import com.example.demo.pojos.User;

public interface UserService {

	public String registerUser(User user);
	
	public User authenticateUser(String email,String password);
	
	public String deleteUser(User user);
	
	public String updateUser(Long id,User newUser);
	
	public List<User> getAllUser();
	
}
