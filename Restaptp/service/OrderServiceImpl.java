package com.example.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.OrderExecutedDao;
import com.example.demo.dao.TransactionDao;
import com.example.demo.pojos.OrderExecuted;
import com.example.demo.pojos.Transaction;
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderExecutedDao orderExecuted;
	
	@Autowired
	TransactionDao transactionDao;
	
	@Override
	public String addOrderExecuted(OrderExecuted order) {
		orderExecuted.save(order);
		return "Order added successfully";
	}

	@Override
	public String addtransaction(Transaction transaction) {
		transactionDao.save(transaction);
		return "Transaction added Successfully"; 
	}

}
