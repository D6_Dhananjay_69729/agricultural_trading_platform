package com.example.demo.service;

import com.example.demo.pojos.OrderExecuted;
import com.example.demo.pojos.Transaction;
import com.example.demo.pojos.User;

public interface OrderService {

	public String addOrderExecuted(OrderExecuted order);
	
	public String addtransaction(Transaction transaction);
}
