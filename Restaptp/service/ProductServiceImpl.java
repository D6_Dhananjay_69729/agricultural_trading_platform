package com.example.demo.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.ProductDao;
import com.example.demo.pojos.Product;

@Transactional
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	
	@Override
	public String addProduct(Product product) {
		productDao.save(product);
		return "product added successfully";
	}

	@Override
	public List<Product> getProductsToBuy() {
		List<Product> listOfProducts= productDao.listOfProducts("buy"); 
		return listOfProducts;
	}

	@Override
	public List<Product> getProductsToSell() {
		List<Product> listOfProducts= productDao.listOfProducts("sell"); 
		return listOfProducts;
	}

	@Override
	public String deleteProduct(Long id) {
		productDao.deleteById(id);
		return "product deleted successfully";
	}

	@Override
	public List<Product> searchByProductNameForBuy(String productName) {
		List<Product>productList=	productDao.productsforBuy(productName);
		return productList;
	}

	@Override
	public List<Product> searchByProductNameForSell(String productName) {
		List<Product>productList=	productDao.productsforSell(productName);
		return productList;
	}

	@Override
	public List<Product> myProducts(long id) {
		List<Product>productList=productDao.myProducts(id);
		return productList;
	}

}
