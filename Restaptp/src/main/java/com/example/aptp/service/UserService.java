package com.example.aptp.service;

import java.util.List;

import com.example.aptp.pojo.Role;
import com.example.aptp.pojo.User;


public interface UserService {

	public String registerUser(User user);
	
	public User authenticateUser(String email,String password);
	
	public String deleteUser(Long id);
	
	public String updateUser(Long id,User newUser);
	
	public List<User> getAllUsers();

	public List<User> findByRole(Role role);
	
}
