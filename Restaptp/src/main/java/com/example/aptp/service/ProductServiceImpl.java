package com.example.aptp.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.aptp.dao.ProductDao;
import com.example.aptp.dao.UserDao;
import com.example.aptp.pojo.Product;
import com.example.aptp.pojo.User;

@Transactional
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	
	@Autowired
	UserDao userDao;
	
	@Override
	public String addProduct(Product product) {
		productDao.save(product);
		return "product added successfully";
	}



	@Override
	public String deleteProduct(Long id) {
		productDao.deleteById(id);
		return "product deleted successfully";
	}

	

	@Override
	public List<Product> myProducts(long id) {
		User user= userDao.findById(id).orElseThrow(()->new RuntimeException("Id not found"));
		List<Product>productList=productDao.findByUser(user);
		return productList;
	}

	@Override
	public List<Product> getAllProducts() {
		
		
		return productDao.findAll();
	}

	@Override
	public List<Product> searchByProductNameForBuy(String productName) {
		List<Product> productToBuy =productDao.listOfProducts("buy", productName);
		return productToBuy;
	}

	@Override
	public List<Product> searchByProductNameForSell(String productName) {
		// TODO Auto-generated method stub
		List<Product> productToSell =productDao.listOfProducts("sell", productName);

		return productToSell;
	}

}
