package com.example.aptp.service;

import java.util.List;

import com.example.aptp.pojo.Product;
import com.example.aptp.pojo.User;


public interface ProductService {

	public String addProduct(Product product);
	
	
	public String deleteProduct(Long id);
	
	public List<Product> searchByProductNameForBuy(String productName);
	
	public List<Product> searchByProductNameForSell(String productName);
	
	public List<Product> myProducts(long id);
	
	public List<Product> getAllProducts();

}
