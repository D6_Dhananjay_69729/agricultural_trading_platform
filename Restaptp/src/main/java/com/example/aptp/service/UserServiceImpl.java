package com.example.aptp.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.aptp.dao.UserDao;
import com.example.aptp.pojo.Role;
import com.example.aptp.pojo.User;

@Transactional
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Override
	public String registerUser(User user) {
		userDao.save(user);
		return "User added successfully";
	}

	@Override
	public User authenticateUser(String email,String password) {
		User user= userDao.AuthenticateUser(email, password);
		return user;
	}

	@Override
	public String deleteUser(Long id) {
		userDao.deleteById(id);
		return "User deleted Successfully";
	}

	@Override
	public String updateUser(Long id,User newUser) {
		User oldUser=userDao.findById(id).orElseThrow(()->new RuntimeException("Id not found"));
		oldUser.setDob(newUser.getDob());
		oldUser.setEmail(newUser.getEmail());
		oldUser.setFirstName(newUser.getFirstName());
		oldUser.setGender(newUser.getGender());
		oldUser.setLastName(newUser.getLastName());
		oldUser.setMiddleName(newUser.getMiddleName());
		oldUser.setMobileNumber(newUser.getMobileNumber());
		oldUser.setPassword(newUser.getPassword());
		userDao.save(oldUser);
		return "user Updates Successfully";
	}

	@Override
	public List<User> getAllUsers() {
		List<User> ulist=userDao.findAll();
		return ulist;
	}

	@Override
	public List<User> findByRole(Role role) {
		// TODO Auto-generated method stub
	     List<User> ulist=userDao.findByRole(role);
		return ulist; 
	}

	
	}
	
	
	

