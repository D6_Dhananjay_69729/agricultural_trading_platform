package com.example.aptp.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.aptp.dao.OrderExecutedDao;
import com.example.aptp.dao.TransactionDao;
import com.example.aptp.pojo.OrderExecuted;
import com.example.aptp.pojo.Transactions;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderExecutedDao orderExecuted;
	
	@Autowired
	TransactionDao transactionDao;
	
	@Override
	public String addOrderExecuted(OrderExecuted order) {
		orderExecuted.save(order);
		return "Order added successfully";
	}

	@Override
	public String addtransaction(Transactions transaction) {
		transactionDao.save(transaction);
		return "Transaction added Successfully"; 
	}

}
