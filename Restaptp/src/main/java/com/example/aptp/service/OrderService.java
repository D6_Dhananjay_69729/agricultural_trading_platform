package com.example.aptp.service;


import com.example.aptp.pojo.OrderExecuted;
import com.example.aptp.pojo.Transactions;

public interface OrderService {

	public String addOrderExecuted(OrderExecuted order);
	
	public String addtransaction(Transactions transaction);
}
