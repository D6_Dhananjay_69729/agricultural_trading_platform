package com.example.aptp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestaptpApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestaptpApplication.class, args);
	}

}
