//package com.example.aptp.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.aptp.pojo.Product;
//import com.example.aptp.pojo.User;
//import com.example.aptp.service.ProductService;
//import com.example.aptp.service.UserService;
//
//@RestController
//@RequestMapping("/seller")
//public class SellerController {
//
//	@Autowired
//	UserService userservice;
//	
//	@Autowired
//	ProductService productservice;
//	
//	@PostMapping("/addprouduct")
//	public String addProduct(@RequestBody Product product) 
//	{
//	return   productservice.addProduct(product);	
//	}
//	
//	@GetMapping("/requireproduct/{productname}")
//	public List<Product>getAllReqProducts(@PathVariable String productname) 
//	{
//		return productservice.searchByProductNameForBuy(productname);
//	}
//	
//	@GetMapping("/myproduct/{id}")
//	public List<Product>getAllProducts(@PathVariable Long id) 
//	{
//		return productservice.myProducts(id);
//	}
//	
//	@PutMapping("/updateseller/{id}")
//	public String updateSeller(@RequestBody User user,@PathVariable Long id )
//	{
//		return userservice.updateUser(id, user);
//	}
//	
//	
//	
//	@DeleteMapping("/deleteproduct/{id}")
//	public String deleteProduct(@PathVariable Long id)
//	{
//		return productservice.deleteProduct(id);
//	}
//	
//	
//	
//	
//	
//}
