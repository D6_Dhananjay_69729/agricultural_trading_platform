//package com.example.aptp.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.aptp.pojo.Product;
//import com.example.aptp.pojo.Role;
//import com.example.aptp.pojo.User;
//import com.example.aptp.service.ProductService;
//import com.example.aptp.service.UserService;
//
//@RestController
//@RequestMapping("/admin")
//public class AdminController {
//	@Autowired
//	private UserService userservice;
//	
//	@Autowired
//	ProductService productservice;
//	
//	//TransactionService transactionService;
//	
//	@GetMapping("/seller/{role}")
//	public List<User> getAllSellers(@PathVariable Role role)
//	{
//		return userservice.findByRole(role);
//	}
//	@GetMapping("/buyer")
//	public List<User> getAllBuyers(@PathVariable Role role)
//	{
//		return userservice.findByRole(role);
//	}
//	@GetMapping("/product")
//	public List<Product> getAllProducts()
//	{
//		return productservice.getAllProducts();
//	}
//	
//	@DeleteMapping("/deleteuser/{id}")
//	public String deleteUser(@PathVariable Long id)
//	{
//		return userservice.deleteUser(id);
//	}
//	
////	@GetMapping("/transactions")
////	public List<User> getAllTransactions()
////	{
////		return userservice.
////	}
//
//}
