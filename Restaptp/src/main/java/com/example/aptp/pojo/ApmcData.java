package com.example.aptp.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "apmcdata")
public class ApmcData extends BaseClass {

	@Column(name = "product_name", length = 50, nullable = false)
	private String productName;

	@Column(name = "season", length = 30, nullable = false)
	private String season;

	@Column(name = "min_price", length = 50, nullable = false)
	private Double minPrice;

	@Column(name = "max_Price", length = 50, nullable = false)
	private Double maxprice;

	@Column(name = "district", length = 30, nullable = false)
	private String district;

}
