package com.example.aptp.pojo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "transactions")
public class Transactions extends BaseClass {

	@Column(name = "trnsaction_number", length = 50, nullable = false)
	private String transactionNumber;
	@Column(name = "trnsaction_amount", length = 50, nullable = false)
	private Double transactionAmount;
	@Column(name = "trnsaction_date",nullable = false)
	private LocalDate transactionDate;
	
	@OneToOne
	@JoinColumn(name = "order_id")
	private OrderExecuted order;
	
}
