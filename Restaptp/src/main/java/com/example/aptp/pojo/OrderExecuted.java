package com.example.aptp.pojo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "orders_tb")
public class OrderExecuted extends BaseClass {

	@Column(name = "order_date",nullable = false)
	private LocalDate orderDate;
	
	@Column(name = "amount",nullable = false)
	private Double amount;
	
	@Column(name = "product_traded",length=30,nullable = false)
	private String productTraded;
	
	@Column(name = "quantity",nullable = false)
	private Integer quantity;
	
	@Column(name = "expected_date",nullable = false)
	private LocalDate expectedDate;
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@OneToOne(mappedBy = "order")
	private Transactions trnsaction;

}
