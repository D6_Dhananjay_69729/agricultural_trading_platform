package com.example.aptp.pojo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Future;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "products")
public class Product extends BaseClass {
	
	@Column(name = "product_name", length= 50, nullable = false)
	private String productName;
	
	@Column(name = "quantity", nullable = false)
	private Integer quantity;
	
	@Column(name = "price", nullable = false)
	private Double price;
	
	@Column(name = "expected_date", nullable = false)@Future
	private LocalDate expectedDate;
	
	@Column(name = "buy_sale", length = 50, nullable = false)
	private String buyOrSale;
	
	@OneToOne
	@JoinColumn(name = "user_id",nullable = false)
	private User user ;
	
	
}
