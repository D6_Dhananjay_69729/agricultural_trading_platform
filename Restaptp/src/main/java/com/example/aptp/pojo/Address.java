package com.example.aptp.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "address")
public class Address extends BaseClass {
	@Column(name = "address",length=150,nullable = false)
	private String address;
	
	@Column(name = "city",length=30,nullable = false)
	private String city;
	
	@Column(name = "district",length=30,nullable = false)
	private String district;
	
	@Column(name = "state",length=30,nullable = false)
	private String state;
	
	@Column(name = "pincode",length=10,nullable = false)
	private String pincode;
	
	@OneToOne
	@JoinColumn(name = "user_id",nullable = false)
	private User user;
}
