package com.example.aptp.pojo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Past;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "users_tb")
public class User extends BaseClass {
	@Column(name = "first_name", length = 20, nullable = false)
	private String firstName;

	@Column(name = "middle_name", length = 20, nullable = false)
	private String middleName;

	@Column(name = "last_name", length = 20, nullable = false)
	private String lastName;

	@Column(length = 10, nullable = false)
	private String title;

	@Column(nullable = false) @Past
	private LocalDate dob;
	
	@Column(name = "reg_date", nullable = false)
	private LocalDate regDate;

	@Enumerated(EnumType.STRING)
	private Role role;

	@Column(name = "email", unique = true, nullable = false,length = 50)
	private String email;

	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "gender", nullable = false, length = 10)
	private String gender;
	
	@Column(name = "contact",length = 14)
	private String mobileNumber; 
	
	@OneToOne(mappedBy = "user")
	private Address address;
	
	@OneToOne(mappedBy = "user")
	private Identity identity;
	
	@OneToOne(mappedBy = "user")
	private BankDetail bankDetail;
	
	@OneToMany(mappedBy = "user")
	private List<OrderExecuted> order = new ArrayList<>();
	
	@OneToMany(mappedBy = "user")
	private List<Product> product = new ArrayList<>();
	

	
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


