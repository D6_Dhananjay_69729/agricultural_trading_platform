package com.example.aptp.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "identity")
public class Identity extends BaseClass {
	
	@Column(name = "photo_id_number", length = 30, nullable = false, unique = true)
	private String photoIdNumber;
	
	@Column(name = "photoId_type", length = 30, nullable = false)
	private String photoIdType;
	
	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
}
