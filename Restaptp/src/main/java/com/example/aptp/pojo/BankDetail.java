package com.example.aptp.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "bank_details")
public class BankDetail extends BaseClass {

	@Column(name = "account_holder", length = 50, nullable = false)
	private String accountHolder;
	
	@Column(name = "bank_name", length = 50, nullable = false)
	private String bankName;
	
	@Column(name = "branch_name", length = 50, nullable = false)
	private String branchName;
	
	@Column(name = "account_number", length = 50, nullable = false,unique = true)
	private String accNumber;
	
	@Column(name = "ifsc_code", length = 50, nullable = false)
	private String ifscCode;
	
	@OneToOne
	@JoinColumn(name = "user_id",nullable = false)
	private User user;
	
}
