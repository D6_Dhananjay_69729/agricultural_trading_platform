package com.example.aptp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.OrderExecuted;
@Repository
public interface OrderExecutedDao extends JpaRepository<OrderExecuted, Long>{

	
}
