package com.example.aptp.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.Transactions;
@Repository
public interface TransactionDao extends JpaRepository<Transactions, Long>{

}
