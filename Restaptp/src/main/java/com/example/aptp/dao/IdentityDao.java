package com.example.aptp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.Identity;

@Repository
public interface IdentityDao extends JpaRepository<Identity, Long> {

}
