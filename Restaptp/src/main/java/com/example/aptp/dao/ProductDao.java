package com.example.aptp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.Product;
import java.lang.String;
import com.example.aptp.pojo.User;

@Repository
public interface ProductDao extends JpaRepository<Product, Long> {

	@Query("select p from Product p where p.buyOrSale =rq and p.productName =pd")
	public List<Product> listOfProducts(@Param("rq")String product,@Param("pd")String productName);
	
	
//	@Query("select p from Product p where p.user_id:mm")
//	public List<Product>myProducts(@Param("mm") Long id);
	
	public List<Product> findByUser(User user);

}
