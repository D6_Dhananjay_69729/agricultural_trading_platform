package com.example.aptp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.Address;
@Repository
public interface AddressDao extends JpaRepository<Address, Long> {

}
