package com.example.aptp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.aptp.pojo.BankDetail;

@Repository
public interface BankDetailDao extends JpaRepository<BankDetail, Long> {

}
