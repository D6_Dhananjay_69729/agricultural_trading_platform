package com.example.demo.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.demo.pojos.Users;
import java.util.List;
import java.lang.String;

@Repository
public interface UserDao extends JpaRepository<Users, Long> {
		
	@Query("select u from Users u where u.email =em and u.password=ps")
	public Users AuthenticateUser( @Param("em") String email,@Param("ps") String password);
	
	public List<Users> findByRole(String role);
	
	public Users findByEmail(String email);
	
	
}
