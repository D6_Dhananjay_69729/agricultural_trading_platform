package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.pojos.Product;
import com.example.demo.pojos.Users;
@Repository
public interface ProductDao extends JpaRepository<Product, Long> {

	@Query("select p from Product p where p.buyOSell =:rq and p.productName=:pd")
	public List<Product> listOfProducts(@Param("rq")String product,@Param("pd")String productName);
	
	
//	@Query("select p from Product p where p.user=:mm")
//	public List<Product>myProducts(@Param("mm") Long id);
	
	List<Product> findByUser(Users user);
	
	
}
