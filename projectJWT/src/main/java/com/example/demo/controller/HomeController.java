package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.pojos.Users;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/")
public class HomeController {

	@Autowired
	UserService userservice;
	
	@PostMapping("/register")
	public String createUser(@RequestBody Users user)
	{
		return userservice.registerUser(user);
	}
}
