package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojos.OrderExecuted;
import com.example.demo.pojos.Product;
import com.example.demo.pojos.Transaction;
import com.example.demo.pojos.Users;
import com.example.demo.service.OrderService;
import com.example.demo.service.ProductService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userService;
	
	@Autowired
	ProductService productservice;
	
	@Autowired
	OrderService orderService;
	
	//TransactionService transactionService;
	
	@GetMapping("/user/{role}")
	public List<Users> getAllSellers(@PathVariable String role)
	{
		return userService.findByRole(role);
	}
	@GetMapping("/product")
	public List<Product> getAllProducts()
	{
		return productservice.getAllProducts();
	}
	
	@DeleteMapping("/deleteuser/{id}")
	public String deleteUser(@PathVariable Long id)
	{
		return userService.deleteUser(id);
	}
	
	@GetMapping("/getallusers")
	public List<Users>getAllUsers(){
		List<Users>listOfusers= userService.getAllUser();
		return listOfusers;
	}
	
	@GetMapping("/getalltransactions")
	public List<Transaction> getAllTransactions()
	{
		List<Transaction> AllTransactions=orderService.getAllTransactions();
		return AllTransactions;
	}

	@GetMapping("/getallorderexecuted")
	public List<OrderExecuted>getAllOrderExecuted(){
		List<OrderExecuted>listOfAllOrderExecuted=orderService.getAllOrderExecuted();
		return listOfAllOrderExecuted;
	}
}
