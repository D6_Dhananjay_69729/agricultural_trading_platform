package com.example.demo.service;

import java.util.List;

import com.example.demo.pojos.Product;

public interface ProductService {

	public String addProduct(Product product,long id);
	
	public String deleteProduct(Long id);
	
	public List<Product> searchByProductNameForBuy(String productName);
	
	public List<Product> searchByProductNameForSell(String productName);
	
	public List<Product> myProducts(long id);
	
	public List<Product> getAllProducts();

	public String deleteProductAfterExecution(long id);

}
