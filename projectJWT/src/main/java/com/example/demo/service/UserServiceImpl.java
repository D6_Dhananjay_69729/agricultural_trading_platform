package com.example.demo.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.UserDao;
import com.example.demo.pojos.Users;

@Transactional
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Override
	public String registerUser(Users user) {
		userDao.save(user);
		return "User added successfully";
	}

	@Override
	public Users authenticateUser(String email,String password) {
		Users user= userDao.AuthenticateUser(email, password);
		return user;
	}

	@Override
	public String deleteUser(long id) {
		userDao.deleteById(id);
		return "User deleted Successfully";
	}

	@Override
	public String updateUser(long id,Users newUser) {
		Users oldUser=userDao.findById(id).orElseThrow(()->new RuntimeException("Id not found"));
		oldUser.setDob(newUser.getDob());
		oldUser.setEmail(newUser.getEmail());
		oldUser.setFirstName(newUser.getFirstName());
		oldUser.setGender(newUser.getGender());
		oldUser.setLastName(newUser.getLastName());
		oldUser.setMiddleName(newUser.getMiddleName());
		oldUser.setMobileNumber(newUser.getMobileNumber());
		oldUser.setPassword(newUser.getPassword());
		userDao.save(oldUser);
		return "user Updates Successfully";
	}

	@Override
	public List<Users> getAllUser() {
		 List<Users>allUsers=userDao.findAll();
		return allUsers;
	}
	
	@Override
	public List<Users> findByRole(String role){
		List<Users>listOfUsers=userDao.findByRole(role);
		return listOfUsers;
	}

	@Override
	public Users findByEmail(String email) {
		// TODO Auto-generated method stub
		return userDao.findByEmail(email);
	}
}
