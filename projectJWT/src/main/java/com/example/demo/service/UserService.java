package com.example.demo.service;

import java.util.List;

import com.example.demo.pojos.Users;

public interface UserService {

	public String registerUser(Users user);
	
	public Users authenticateUser(String email,String password);
	
	public String deleteUser(long id);
	
	public String updateUser(long id,Users newUser);
	
	public List<Users> getAllUser();

	public List<Users> findByRole(String role);
	
	public Users findByEmail(String email);
}
