package com.example.demo.service;

import java.time.LocalDate;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.dao.ProductDao;
import com.example.demo.dao.UserDao;
import com.example.demo.pojos.OrderExecuted;
import com.example.demo.pojos.Product;
import com.example.demo.pojos.Transaction;
import com.example.demo.pojos.Users;

@Transactional
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductDao productDao;
	@Autowired
	UserDao userDao;
	@Autowired
	OrderService orderService;
	
	@Override
	public String addProduct(Product product,long id) {
		Users user=userDao.findById(id).orElseThrow(()->new RuntimeException("user not found"));
		product.setUser(user);
		productDao.save(product);
		return "product added successfully";
	}


	@Override
	public String deleteProduct(Long id) {
		productDao.deleteById(id);
		return "product deleted successfully";
	}


	@Override
	public List<Product> myProducts(long id) {
		Users user=userDao.findById(id).orElseThrow(()->new RuntimeException("user not found"));
		List<Product>myProductList=productDao.findByUser(user);
		return myProductList;
	}

	@Override
	public List<Product> getAllProducts() {
		List<Product>allProducts=productDao.findAll();
		return allProducts;
	}


	@Override
	public List<Product> searchByProductNameForBuy(String productName) {
		List<Product> productsToBuy=productDao.listOfProducts("buy", productName);
		return productsToBuy;
	}


	@Override
	public List<Product> searchByProductNameForSell(String productName) {
		List<Product> productsToSell=productDao.listOfProducts("Sell", productName);
		return productsToSell;
	}

	@Override
	public String deleteProductAfterExecution(long id) {
		Product product=productDao.findById(id).orElseThrow(()->new RuntimeException("Product not found"));
		OrderExecuted orderExecuted=new OrderExecuted();
		double total=product.getPrice()*product.getQuantity();
		orderExecuted.setAmount(total);
		orderExecuted.setExpectedDate(product.getExpectedDate());
		orderExecuted.setProductTraded(product.getProductName());
		orderExecuted.setQuantity(product.getQuantity());
		orderExecuted.setOrderDate(LocalDate.now());
		orderExecuted.setUser(product.getUser()); 
		Transaction transaction=new Transaction();
		transaction.setAmount(total);
		transaction.setTrnDate(LocalDate.now());
		transaction.setOrderExecuted(orderExecuted);
		transaction.setTrnNumber(orderService.generateRandomString());
		orderExecuted.setTransaction(transaction);
		orderService.addOrderExecuted(orderExecuted);
		productDao.deleteById(product.getId());
		return "Order placed successfully";
	}

}
