package com.example.demo.pojos;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "order_executed")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(("user"))
public class OrderExecuted extends BaseClass {

	@Column(name = "order_date",nullable = false)
	private LocalDate orderDate;
	@Column(name = "amount",nullable = false)
	private double amount;
	@Column(name = "product_traded",nullable = false,length = 30)
	private String productTraded;
	@Column(name = "quantity",nullable = false)
	private double quantity;
	@Column(name = "expected_date",nullable = false)
	private LocalDate expectedDate;
	@OneToOne @JoinColumn(name="user_id")
	private Users user;
	@OneToOne(mappedBy = "orderExecuted", cascade = CascadeType.ALL)
	private Transaction transaction;
	@Override
	public String toString() {
		return "OrderExecuted [orderDate=" + orderDate + ", amount=" + amount + ", productTraded=" + productTraded
				+ ", quantity=" + quantity + ", expectedDate=" + expectedDate + "]";
	}
	
	
}
