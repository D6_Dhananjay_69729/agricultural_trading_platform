package com.example.demo.pojos;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="users")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Users extends BaseClass {

	@Column(name="title",length = 10)
	private String title;
	@Column(name = "first_name",length = 20,nullable = false)
	private String firstName;
	@Column(name = "middle_name",length = 20,nullable = false)
	private String middleName;
	@Column(name = "last_name",length = 20,nullable = false)
	private String lastName;
	@Column(name = "dob",nullable = false)
	private LocalDate dob;
	@Column(name = "registration_date",nullable = false)
	private LocalDate registrationDate;
	@Column(name = "role",nullable = false,length = 25)
	private String role;
	@Column(name = "password",nullable = false)
	private String password;
	@Column(name = "gender",length = 20,nullable = false)
	private String gender;
	@Column(name = "email",length = 40,nullable = false)
	private String email;
	@Column(name = "mobile_number",length = 14,nullable = false)
	private String mobileNumber;
	
	//relation with product pojo
	@OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
	private List<Product>product;
	
	//relation with orderExecuted pojo 
	@OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
	private List<OrderExecuted>orderExecuted;
	
	//relation with address pojo 
	@OneToOne(cascade = CascadeType.ALL)
	private Address address;
	//relation with identity pojo 
	@OneToOne(cascade = CascadeType.ALL)
	private Identity identity;
	//relation with BankDetail pojo 
	@OneToOne(cascade = CascadeType.ALL)
	private BankDetail bankDetails;
	
	
	
	// Customized constructor
	public Users(String title, String firstName, String middleName, String lastName, LocalDate dob,
			LocalDate registrationDate, String role, String password, String gender, String email,
			String mobileNumber) {
		super();
		this.title = title;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dob = dob;
		this.registrationDate = registrationDate;
		this.role = role;
		this.password = password;
		this.gender = gender;
		this.email = email;
		this.mobileNumber = mobileNumber;
	}
	
	// To String method
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", dob=" + dob
				+ ", registrationDate=" + registrationDate + ", role=" + role + ", password=" + password + ", gender="
				+ gender + ", email=" + email + ", mobileNumber=" + mobileNumber + "]";
	}
	
	public User toUser() {
			SimpleGrantedAuthority auth = new SimpleGrantedAuthority(role);
			return new User(email, password, Collections.singletonList(auth));
		
	}
}
