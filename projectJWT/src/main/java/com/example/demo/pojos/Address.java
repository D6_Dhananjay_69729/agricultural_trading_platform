package com.example.demo.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="address")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Address extends BaseClass{
	
	@Column(name = "address",nullable = false,length = 150)
	private String address;
	@Column(name = "village",nullable = false,length = 30)
	private String village;
	@Column(name = "district",nullable = false,length = 30)
	private String district;
	@Column(name = "state",nullable = false,length = 30)
	private String state;
	@Column(name = "pincode",nullable = false,length = 12)
	private String pincode;
//	@OneToOne  @JoinColumn(name = "user_id",nullable = false) 
//	private User user;
	@Override
	public String toString() {
		return "Address [address=" + address + ", village=" + village + ", district=" + district + ", state=" + state
				+ ", pincode=" + pincode +"]";
	}
	
}
