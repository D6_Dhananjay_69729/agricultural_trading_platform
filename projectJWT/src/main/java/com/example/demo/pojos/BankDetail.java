package com.example.demo.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="bank_details")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BankDetail extends BaseClass{

	@Column(name = "bank_name",nullable = false,length = 40)
	private String bankName;
	@Column(name = "account_holder",nullable = false,length = 45)
	private String accountHolderName;
	@Column(name = "branch",nullable = false,length = 20)
	private String branch;
	@Column(name = "account_number",nullable = false,length = 30,unique = true)
	private String accountNumber;
	@Column(name = "ifsc_code",nullable = false,length = 30)
	private String ifscCode;

	@Override
	public String toString() {
		return "BankDetail [bankName=" + bankName + ", accountHolderName=" + accountHolderName + ", branch=" + branch
				+ ", accountNumber=" + accountNumber + ", ifscCode=" + ifscCode + "]";
	}
	
}
