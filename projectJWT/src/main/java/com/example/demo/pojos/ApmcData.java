package com.example.demo.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "apmc_data")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApmcData extends BaseClass{
	
	@Column(name = "product_name",nullable = false ,unique = true,length = 30)
	private String productName;
	@Column(name = "season",nullable = false,length = 20)
	private String season;
	@Column(name = "minimum_price")
	private double minPrice;
	@Column(name = "maximum_price")
	private double maxPrice;
	@Column(name = "district",length = 30)
	private String district;
		
	@Override
	public String toString() {
		return "ApmcData [productName=" + productName + ", season=" + season + ", minPrice=" + minPrice + ", maxPrice="
				+ maxPrice + ", district=" + district + "]";
	}
	
}
